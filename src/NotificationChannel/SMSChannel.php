<?php

namespace Baghban\SMS\NotificationChannel;

use Illuminate\Notifications\Notification;
use \SMS;

class SMSChannel
{

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSMS($notifiable);
        SMS::sendSMS((array) $message->recipient, $message->content);
    }
}