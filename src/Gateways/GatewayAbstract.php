<?php

namespace Baghban\SMS\Gateways;


abstract class GatewayAbstract implements GatewayInterface {
	public $webService;
	public $username;
	public $password;
	public $from;
	public $key;
}