<?php

namespace Baghban\SMS\Gateways;

use Baghban\SMS\Exceptions\CouldNotSendSMS;

class MelipayamakGateway extends GatewayAbstract {

	/**
	 * AdpdigitalGateway constructor.
	 */
	public function __construct() {

		$this->webService  = config('sms.gateway.melipayamak.webService');
		$this->username    = config('sms.gateway.melipayamak.username');
		$this->password    = config('sms.gateway.melipayamak.password');
		$this->from        = config('sms.gateway.melipayamak.from');
	}


	/**
	 * @param array $numbers
	 * @param       $text
	 * @param bool  $isflash
	 *
	 * @return mixed
	 * @internal param $to | array
	 */
	public function sendSMS( array $numbers, $text, $isflash = false ) {
		try {
			$client = new \SoapClient( $this->webService );
			$result = $client->SendSMS(
				[
					'username' => $this->username,
					'password' => $this->password,
					'from'     => $this->from,
					'to'       => $numbers,
					'text'     => $text,
					'isflash'  => $isflash,
					'udh'      => '',
					'recId'    => [ 0 ],
					'status'   => 0x0,
				]
			);
			$result = $result->SendSmsResult;
			if($result == 0) {
				throw CouldNotSendSMS::authenicationFailed();
			} elseif($result == 2) {
				throw CouldNotSendSMS::credisNotEnough();
			} elseif($result != 1) {
				throw CouldNotSendSMS::sendSMSWasUnsuccessful("Send SMS  was unsuccessful");
			}
		} catch( \SoapFault $e ) {
			throw CouldNotSendSMS::gatewayRespondedWithAnError($e->getMessage());
		}
	}


	/**
	 * @return mixed
	 */
	public function getCredit() {
		try {
			$client = new \SoapClient( $this->webService );

			$result = $client->GetCredit( [
				"username" => $this->username,
				"password" => $this->password,
			] )->GetCreditResult;
			if($result == 0) {
				throw CouldNotSendSMS::authenicationFailed();
			}
			return $result;
		} catch( \SoapFault $e ) {
			throw CouldNotSendSMS::gatewayRespondedWithAnError($e->getMessage());
		}
	}

}