<?php
namespace Baghban\SMS\Exceptions;
class CouldNotSendSMS extends \Exception
{
    /**
     * Thrown when there's a bad request and an error is responded.
     *
     * @param string $message
     * @return static
     */
    public static function gatewayRespondedWithAnError($message)
    {
        return new static($message);
    }

    /**
     * Thrown when send SMS result is unsuccessful.
     *
     * @param string $message
     * @return static
     */
    public static function sendSMSWasUnsuccessful($message)
    {
        return new static($message);
    }

    /**
     * Thrown when authenication failed.
     *
     * @param string $message
     * @return static
     */
    public static function authenicationFailed($message = "Authenication failed.")
    {
        return new static($message);
    }

    /**
     * Thrown when credit is not enough.
     *
     * @param string $message
     * @return static
     */
    public static function creditIsNotEnough($message = "Credit is not enough.")
    {
        return new static($message);
    }


}