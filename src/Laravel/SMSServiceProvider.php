<?php

namespace Baghban\SMS\Laravel;

use Illuminate\Support\ServiceProvider;
use Baghban\SMS\SMS;

class SMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
	    $this->publishes([
		    __DIR__.'/config/sms.php' => config_path('sms.php'),
	    ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->mergeConfigFrom(
		    __DIR__.'/config/sms.php', 'sms'
        );
        
        app()->bind('SMS', function() {
            return new \Baghban\SMS\SMS;
        });
    }
}
