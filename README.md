# SMS
Package for send sms and notification with laravel  (All gatways in Iran).
This package originally forked from [LaravelSms](https://github.com/pamenary/LaravelSms/)

installation
------------
For install this package Edit your project's ```composer.json``` file to require baghban/sms

```php
"require": {
  "baghban/sms": "dev-master"
},
```

Now, update Composer:
```
composer update
```